var $$ = Dom7;
var app = new Framework7({
  // App root element
  el: '#app',
  theme:'md',
  // App Name
  name: 'SI TORTOR',
  // App id
  id: 'io.partopitao.sitortor',
  // Enable swipe panel
  panel: {
    swipe: true,
  },
  routes: routes
});
var viewHome = app.views.create('#view-home', {name:'home', main:true});
var viewProject = app.views.create('#view-project', {name:'project'});
var viewProfile = app.views.create('#view-profile', {name:'profile'});

$(document).on('click', '.route-tab-link', function (e) {
  var url = $(this).attr('href');
  var view = app.views.get($(this).attr('data-view'));
  var tabId = $(this).attr('data-tab');
  app.tab.show(tabId);
  view.router.navigate(url);
});

/*viewHome.on('view:init', function(){});
viewProject.on('view:init', function(){});
viewProfile.on('view:init', function(){});*/

$$("#view-home" ).on('tab:show', function(ui) {
  var auth = getLocalStorage('auth');
  if (auth) {
    $$('.el-hide-login', ui.target).hide();
    $$('.el-show-login', ui.target).show();

    $$('[data-label=name]', ui.target).html(auth.Nama);
    $$('[data-label=email]', ui.target).html(auth.Role);

    getData('projects',{UserName: auth.Email}, function(res) {
      res = JSON.parse(res);
      if(res.error == 0) {
        setLocalStorage('projects', res.data);
        drawProjects($('[data-label=list-project]', ui.target), 'projects',5);
      }
    }, function(){}, function(){});

    getData('notification',{UserName: auth.Email}, function(res) {
      res = JSON.parse(res);
      if(res.error == 0) {
        if(res.data && res.data.length>0) {
          setLocalStorage('notification', res.data);
          var notifs = '';
          notifs += '<ul>';
          notifs += '<li>';
          notifs += '<a href="/notification/" class="item-link item-content" data-force="true" data-ignore-cache="true">';
          notifs += '<div class="item-media">';
          notifs += '<img src="img/icon-notification.png" width="60" />';
          notifs += '</div>';
          notifs += '<div class="item-inner">';
          notifs += '<div class="item-title-row">';
          notifs += '<div class="item-title">Peringatan</div>';
          notifs += '</div>';
          //notifs += '<div class="item-subtitle">'+res.data[i].MessageSeverity+'</div>';
          notifs += '<div class="item-text"><strong>Terdapat <strong>'+res.data.length+'</strong> pada project / tower yang sedang berjalan. Klik untuk melihat lebih rinci</div>';
          notifs += '</div>';
          notifs += '</a>';
          notifs += '</li>';
          notifs += '</ul>';
          //$$('[data-label=label-notification]', ui.target).show();
          $$('[data-label=list-notification]', ui.target).removeClass('skeleton-text skeleton-effect-wave').html(notifs).show();
        } else {
          //$$('[data-label=label-notification]', ui.target).hide();
          $$('[data-label=list-notification]', ui.target).removeClass('skeleton-text skeleton-effect-wave').empty().hide();
        }
      }
    }, function(){}, function(){});
  } else {
    app.view.current.router.navigate('/login/',{reloadCurrent: true,ignoreCache: true,});
    return false;
  }
});

$$("#view-project" ).on('tab:show', function(ui) {
  var auth = getLocalStorage('auth');

  if(auth) {
    if(auth.ID_Role == ROLE_ADMIN || auth.ID_Role == ROLE_PENGGUNA) {
      $$('[data-label=btn-project-add]').show();
    }
    getData('projects',{UserName: auth.Email}, function(res) {
      res = JSON.parse(res);
      if(res.error == 0) {
        setLocalStorage('projects', res.data);
        drawProjects($('[data-label=list-project]', ui.target), 'projects');
      }
    }, function(){}, function(){});
  }
});

$$("#view-profile" ).on('tab:show', function(ui) {
  var auth = getLocalStorage('auth');
  $$('[data-label=input-name]', ui.target).val(auth.Nama);
  $$('[data-label=input-role]', ui.target).val(auth.Role);
  $$('[data-label=input-email]', ui.target).val(auth.Email);

  $('[data-label=btn-logout]', ui.target).unbind('click').click(function(){
    app.dialog.confirm('Apakah anda yakin ingin logout dari aplikasi?', 'Logout', function(){
      localStorage.clear();
      location.reload();
    }, function(){

    });
    return false;
  });

  $('[data-label=btn-submit]', ui.target).unbind('click').click(function(){
    app.dialog.confirm('Apakah anda yakin ingin memperbarui profil anda?', 'Profil', function(){
      var name = $$('[data-label=input-name]', ui.target).val();
      getData('changeprofile', {username: auth.Email, name: name}, function(res) {
        res = JSON.parse(res);
        if(res.error == 0) {
          var toast = app.toast.create({
            text: res.success,
            icon: '<i class="icon material-icons md-only">check_circle</i>',
            position: 'center',
            closeTimeout: 2000
          });
          toast.open();
          setLocalStorage('auth', res.data);
          app.view.current.router.refreshPage();
        } else {
          app.dialog.alert(res.error);
          return false;
        }
      }, function(){
        var toast = app.toast.create({
          text: 'Terjadi kesalahan pada server. Silakan coba kembali.',
          icon: '<i class="icon material-icons md-only">error</i>',
          position: 'center',
          closeTimeout: 2000
        });
        toast.open();
      }, function(){
        app.preloader.hide();
      });
    }, function(){

    });
    return false;
  });
});

$$(document).on('page:init', '.page[data-name="login"]', function (e, page) {
  var el = page.el;
  $('.button-login', el).unbind('click').click(function(){
    var username_ = $$('[name=username]', el).val();
    var password_ = $$('[name=password]', el).val();

    app.preloader.show();
    getData('login', {username: username_, password: password_}, function(res) {
      res = JSON.parse(res);
      if(res.error == 0) {
        setLocalStorage('auth', res.data);
        app.view.current.router.navigate('/',{reloadCurrent: true,ignoreCache: true,});
      }
      else {
        app.dialog.alert(res.error);
      }
    }, function() {
      app.dialog.alert("Terjadi kesalahan. Silakan coba kembali atau hubungi administrator.");
    }, function() {
      app.preloader.hide();
    });
  });
});

$$(document).on('page:afterin', '.page[data-name="home"]', function (e, page) {
  var el = page.el;
  var auth = getLocalStorage('auth');

  if (auth) {
    $$('.el-hide-login', el).hide();
    $$('.el-show-login', el).show();

    $$('[data-label=name]', el).html(auth.Nama);
    $$('[data-label=email]', el).html(auth.Role);

    getData('projects',{UserName: auth.Email}, function(res) {
      res = JSON.parse(res);
      if(res.error == 0) {
        setLocalStorage('projects', res.data);
        drawProjects($('[data-label=list-project]', el), 'projects',5);
      }
    }, function(){}, function(){});

    getData('notification',{UserName: auth.Email}, function(res) {
      res = JSON.parse(res);
      if(res.error == 0) {
        if(res.data && res.data.length>0) {
          setLocalStorage('notification', res.data);
          var notifs = '';
          notifs += '<ul>';
          notifs += '<li>';
          notifs += '<a href="/notification/" class="item-link item-content" data-force="true" data-ignore-cache="true">';
          notifs += '<div class="item-media">';
          notifs += '<img src="img/icon-notification.png" width="45" />';
          notifs += '</div>';
          notifs += '<div class="item-inner">';
          notifs += '<div class="item-title-row">';
          notifs += '<div class="item-title text-color-orange">Peringatan</div>';
          notifs += '<div class="item-after text-color-orange" style="font-size: 14pt !important; font-weight: bold">'+res.data.length+'</div>';
          notifs += '</div>';
          //notifs += '<div class="item-subtitle">'+res.data[i].MessageSeverity+'</div>';
          notifs += '<div class="item-text">Klik untuk melihat lebih lengkap.</div>';
          notifs += '</div>';
          notifs += '</a>';
          notifs += '</li>';
          notifs += '</ul>';
          //$$('[data-label=label-notification]', ui.target).show();
          $$('[data-label=list-notification]', el).removeClass('skeleton-text skeleton-effect-wave').html(notifs).show();
        } else {
          //$$('[data-label=label-notification]', ui.target).hide();
          $$('[data-label=list-notification]', el).removeClass('skeleton-text skeleton-effect-wave').empty().hide();
        }
      }
    }, function(){}, function(){});
  } else {
    app.view.current.router.navigate('/login/',{reloadCurrent: true,ignoreCache: true,});
    return false;
  }

  $('[data-label=btn-logout]', el).unbind('click').click(function(){
    app.dialog.confirm('Apakah anda yakin ingin logout dari aplikasi?', 'Logout', function(){
      localStorage.clear();
      app.view.current.router.navigate('/login/',{reloadCurrent: true,ignoreCache: true,});
    }, function(){

    });
    return false;
  });
});

$$(document).on('page:afterin', '.page[data-name="projects"]', function (e, page) {
  var el = page.el;
  var auth = getLocalStorage('auth');

  if(auth) {
    getData('projects',{UserName: auth.Email}, function(res) {
      res = JSON.parse(res);
      if(res.error == 0) {
        setLocalStorage('projects', res.data);
        drawProjects($('[data-label=list-project]', el), 'projects');
      }
    }, function(){}, function(){});
  }

});

$$(document).on('page:afterin', '.page[data-name="project"]', function (e, page) {
  var el = page.el;
  var param = page.route.params;
});

$$(document).on('page:afterin', '.page[data-name="project-form"]', function (e, page) {
  var el = page.el;
  var param = page.route.params;
  var auth = getLocalStorage('auth');

  getData('get-list-member', {}, function(res) {
    res = JSON.parse(res);
    if(res.error == 0) {
      if(res.data) {
        $$('[data-label=list-tower]', el).empty();
        $$('[data-label=list-tower]', el).removeClass('skeleton-text skeleton-effect-wave');
        var members = '<ul>';
        if(res.data.length > 0) {
          for(var i=0; i<res.data.length; i++) {
            members += '<li>';
            members += '<label class="item-checkbox item-content">';
            members += '<input type="checkbox" name="Members" value="'+res.data[i].UserName+'" />';
            members += '<i class="icon icon-checkbox"></i>';
            members += '<div class="item-inner">';
            members += '<div class="item-title">'+res.data[i].Name+'</div>';
            members += '</div>';
            members += '</label>';
            members += '</li>';
          }
        } else {
          members += '<li class="item-divider" style="font-style: italic">Tidak ada data tersedia.</li>';
        }
        members += '</ul>';
        $$('[data-label=list-member]', el).html(members);
      }
    } else {

    }
  }, function(){
    var toast = app.toast.create({
      text: 'Gagal memuat daftar anggota.',
      icon: '<i class="icon material-icons md-only">error</i>',
      position: 'center',
      closeTimeout: 2000
    });
    toast.open();
  }, function(){
  });

  $('[data-label=btn-submit]', el).unbind('click').click(function(){
    if(!app.input.validateInputs($('form[data-label=form]', el))) {
      app.dialog.alert('Harap isi form dengan benar.');
      return false;
    }

    var members = [];
    var memberChecked = $('[name=Members]:checked', el);
    if(!memberChecked || memberChecked.length==0) {
      app.dialog.alert('Harap memilih minimal 1 (satu) orang anggota.');
      return false;
    }

    for (var i=0; i<memberChecked.length; i++) {
      members.push($(memberChecked[i]).val());
    }

    var member_ = members.join();
    var input = {
      UserName: auth.Email,
      PrName: $('[name=PrName]', el).val(),
      PrContractor: $('[name=PrContractor]', el).val(),
      PrTrans: $('[name=PrTrans]', el).val(),
      PrTransLength: $('[name=PrTransLength]', el).val(),
      Members: member_
    }
    app.preloader.show();
    getData('project-create', input, function(res) {
      res = JSON.parse(res);
      if(res.error == 0) {
        if(res.data) setLocalStorage('projects', res.data);

        drawProjects($('[data-label=list-project]', el), 'projects', 5);
        app.view.current.router.navigate('/',{reloadCurrent: true,ignoreCache: true});
      } else {
        app.dialog.alert(res.error);
        return false;
      }
    }, function(){

    }, function(){
      app.preloader.hide();
    });
  });
});
$$(document).on('sortable:sort', '[data-label=list-tower]', function (e, el){
  console.log(e);
  var uniq = $(e.target).data('uniq');
  var from_ = e.detail.from;
  var to_ = e.detail.to;

  app.preloader.show();
  getData('tower-seq-update/'+uniq, {from: from_, to: to_}, function(res) {
    res = JSON.parse(res);
    if(res.error == 0) {

    } else {
      app.dialog.alert(res.error);
      return false;
    }
  }, function(){

  }, function(){
    app.preloader.hide();
    app.view.current.router.refreshPage();
  });
});
$$(document).on('page:afterin', '.page[data-name="project-detail"]', function (e, page) {
  var el = page.el;
  var param = page.route.params;
  var projects = getLocalStorage('projects');
  var auth = getLocalStorage('auth');

  $$('[data-label=btn-delete]').hide();
  $$('[data-label=btn-tower-add]').closest('.list').hide();
  $$('[data-label=btn-tower-add]').attr('href', '/tower-form/'+param.id+'/');

  $$('[data-label=btn-download-report]', el).click(function(){
    var ref = cordova.InAppBrowser.open(URL_API+'report/8', '_system', 'location=yes');
    /*var fileTransfer = new FileTransfer();
    var uri = encodeURI('https://chayrasmart.com/sitortor/site/api/report/'+param.id);
    fileTransfer.download(
      uri,
      cordova.file.externalApplicationStorageDirectory+'Laporan-'+moment().format('YYYY-MM-DD')+'.pdf',
      function(entry) {
        var toast = app.toast.create({
          text: 'Dokumen berhasil diunduh ke '+cordova.file.externalApplicationStorageDirectory,
          icon: '<i class="icon material-icons md-only">check</i>',
          position: 'center',
          closeTimeout: 2000
        });
        toast.open();
      },
      function(error) {
        var toast = app.toast.create({
          text: 'Dokumen gagal diunduh. Silakan coba kembali.',
          icon: '<i class="icon material-icons md-only">error</i>',
          position: 'center',
          closeTimeout: 2000
        });
        toast.open();
        console.log("download error source " + error.source);
        console.log("download error target " + error.target);
        console.log("download error code" + error.code);
        console.log(error.body);
        console.log(error.exception);
      },
      true
    );*/
    return false;
  });

  $('[data-label=btn-delete]', el).unbind('click').click(function(){
    app.dialog.confirm('Apakah anda yakin ingin menghapus project ini?', 'Hapus', function(){
      app.preloader.show();
      getData('project-delete/'+param.id, {}, function(res) {
        res = JSON.parse(res);
        if(res.error == 0) {
          app.view.current.router.back();
        } else {
          app.dialog.alert(res.error);
          return false;
        }
      }, function(){
        var toast = app.toast.create({
          text: 'Terjadi kesalahan pada server. Silakan coba kembali.',
          icon: '<i class="icon material-icons md-only">error</i>',
          position: 'center',
          closeTimeout: 2000
        });
        toast.open();
      }, function(){
        app.preloader.hide();
      });
    });
    return false;
  });

  $('[data-label=btn-filter]', el).unbind('click').click(function(){
    var val_ = $$('[data-label=list-tower]', el).attr('data-filter-code');
    if(!val_) {
      val_ = -1;
    }

    var opts = '';
    opts += '<li>';
    opts += '<label class="item-radio item-radio-icon-start item-content">';
    opts += '<input type="radio" name="radio" value="-1" '+(val_==-1?'checked':'')+' />';
    opts += '<i class="icon icon-radio"></i><div class="item-inner"><div class="item-title">SEMUA</div></div>';
    opts += '</label>';
    opts += '</li>';
    opts += '<li>';
    opts += '<label class="item-radio item-radio-icon-start item-content">';
    opts += '<input type="radio" name="radio" value="1" '+(val_==1?'checked':'')+' />';
    opts += '<i class="icon icon-radio"></i><div class="item-inner"><div class="item-title">Belum Bebas</div></div>';
    opts += '</label>';
    opts += '</li>';
    opts += '<li>';
    opts += '<label class="item-radio item-radio-icon-start item-content">';
    opts += '<input type="radio" name="radio" value="2" '+(val_==2?'checked':'')+' />';
    opts += '<i class="icon icon-radio"></i><div class="item-inner"><div class="item-title">Idle Pondasi</div></div>';
    opts += '</label>';
    opts += '</li>';
    opts += '<li>';
    opts += '<label class="item-radio item-radio-icon-start item-content">';
    opts += '<input type="radio" name="radio" value="3" '+(val_==3?'checked':'')+' />';
    opts += '<i class="icon icon-radio"></i><div class="item-inner"><div class="item-title">On Going Pondasi</div></div>';
    opts += '</label>';
    opts += '</li>';
    opts += '<li>';
    opts += '<label class="item-radio item-radio-icon-start item-content">';
    opts += '<input type="radio" name="radio" value="4,5,6,7" '+(val_=='4,5,6,7'?'checked':'')+' />';
    opts += '<i class="icon icon-radio"></i><div class="item-inner"><div class="item-title">Pondasi Selesai</div></div>';
    opts += '</label>';
    opts += '</li>';
    opts += '<li>';
    opts += '<label class="item-radio item-radio-icon-start item-content">';
    opts += '<input type="radio" name="radio" value="4" '+(val_==4?'checked':'')+' />';
    opts += '<i class="icon icon-radio"></i><div class="item-inner"><div class="item-title">Masa Tunggu Beton</div></div>';
    opts += '</label>';
    opts += '</li>';
    opts += '<li>';
    opts += '<label class="item-radio item-radio-icon-start item-content">';
    opts += '<input type="radio" name="radio" value="5" '+(val_==5?'checked':'')+' />';
    opts += '<i class="icon icon-radio"></i><div class="item-inner"><div class="item-title">Idle Erection</div></div>';
    opts += '</label>';
    opts += '</li>';
    opts += '<li>';
    opts += '<label class="item-radio item-radio-icon-start item-content">';
    opts += '<input type="radio" name="radio" value="6" '+(val_==6?'checked':'')+' />';
    opts += '<i class="icon icon-radio"></i><div class="item-inner"><div class="item-title">On Going Erection</div></div>';
    opts += '</label>';
    opts += '</li>';
    opts += '<li>';
    opts += '<label class="item-radio item-radio-icon-start item-content">';
    opts += '<input type="radio" name="radio" value="7" '+(val_==7?'checked':'')+' />';
    opts += '<i class="icon icon-radio"></i><div class="item-inner"><div class="item-title">Selesai</div></div>';
    opts += '</label>';
    opts += '</li>';

    var content = '<div class="dialog-input-field input margin-top"><div class="item-input-wrap"><div class="list"><ul>'+opts+'</ul></div></div></div>';
    var dialogForm = app.dialog.create({
      closeByBackdropClick: true,
      destroyOnClose: true,
      title: 'Daftar Tower',
      text: 'Filter berdasarkan status : ',
      content: content,
      buttons: [
        {text: 'BATAL', close: true, cssClass: 'color-gray'},
        {
          text: 'OK',
          close: false,
          onClick: function(dialog, e){
            var val_ = $$('input[type=radio]:checked', dialog.el).val();
            if(!val_) {
              val_ = -1;
            }

            $('[data-label=list-tower]', el).attr('data-filter-code', val_);
            $('li', $('[data-label=list-tower]', el)).show();
            if(val_ != -1) {
              var arrsplit = val_.split(',');
              if(arrsplit.length>1) {
                $('li', $('[data-label=list-tower]', el)).hide();
                for(var l=0; l<arrsplit.length; l++) {
                  $('li[data-statuscode='+arrsplit[l]+']', $('[data-label=list-tower]', el)).show();
                }
              } else if(arrsplit[0]) {
                $('li[data-statuscode!='+arrsplit[0]+']', $('[data-label=list-tower]', el)).hide();
              }
            }

            dialog.close();
          }
        }
      ]
    });
    dialogForm.open();
    return false;
  });

  if(projects) {
    var det = projects.find(x => x.Uniq == param.id);
    $$('[data-label=title]', el).removeClass('skeleton-text skeleton-effect-wave').html(det.PrName);
    $$('[data-label=pr-contractor]', el).removeClass('skeleton-text skeleton-effect-wave').html(det.PrContractor);
    $$('[data-label=pr-trans]', el).removeClass('skeleton-text skeleton-effect-wave').html(det.PrTrans);
    $$('[data-label=pr-translength]', el).removeClass('skeleton-text skeleton-effect-wave').html(det.PrTransLength);
  }

  getData('project-detail/'+param.id, {UserName: auth.Email}, function(res) {
    res = JSON.parse(res);
    if(res.error == 0) {
      if(res.data.isAuthorized) {
        $$('[data-label=btn-delete]').show();
        $$('[data-label=btn-tower-add]').closest('.list').show();
      }

      if(res.data && res.data.project) {
        $$('[data-label=title]', el).removeClass('skeleton-text skeleton-effect-wave').html(res.data.project.PrName);
        $$('[data-label=pr-contractor]', el).removeClass('skeleton-text skeleton-effect-wave').html(res.data.project.PrContractor);
        $$('[data-label=pr-trans]', el).removeClass('skeleton-text skeleton-effect-wave').html(res.data.project.PrTrans);
        $$('[data-label=pr-translength]', el).removeClass('skeleton-text skeleton-effect-wave').html(res.data.project.PrTransLength);
        $$('[data-label=pr-progress]', el).removeClass('skeleton-text skeleton-effect-wave').html(numeral(parseFloat(res.data.project.progressPercentage)).format('0,00')+'%');

        $$('[data-label=sum-total]', el).removeClass('skeleton-text skeleton-effect-wave').html(res.data.project.sumAll);
        $$('[data-label=sum-tower-selesai]', el).removeClass('skeleton-text skeleton-effect-wave').html(res.data.project.sumTowerSelesai);
        $$('[data-label=sum-pondasi-selesai]', el).removeClass('skeleton-text skeleton-effect-wave').html(res.data.project.sumPondasiSelesai);
        $$('[data-label=sum-erection-selesai]', el).removeClass('skeleton-text skeleton-effect-wave').html(res.data.project.sumErectionSelesai);
        $$('[data-label=sum-tower-belum]', el).removeClass('skeleton-text skeleton-effect-wave').html(res.data.project.sumTowerBelum);
        $$('[data-label=sum-pondasi-belum]', el).removeClass('skeleton-text skeleton-effect-wave').html(res.data.project.sumPondasiBelum);
        $$('[data-label=sum-erection-belum]', el).removeClass('skeleton-text skeleton-effect-wave').html(res.data.project.sumErectionBelum);
        $$('[data-label=sum-pondasi-ongoing]', el).removeClass('skeleton-text skeleton-effect-wave').html(res.data.project.sumPondasiOngoing);
        $$('[data-label=sum-erection-ongoing]', el).removeClass('skeleton-text skeleton-effect-wave').html(res.data.project.sumErectionOngoing);
        $$('[data-label=sum-pondasi-idle]', el).removeClass('skeleton-text skeleton-effect-wave').html(res.data.project.sumPondasiAvailable);
        $$('[data-label=sum-erection-idle]', el).removeClass('skeleton-text skeleton-effect-wave').html(res.data.project.sumErectionAvailable);
      }

      /* List Member */
      $$('[data-label=list-member]', el).empty();
      $$('[data-label=list-member]', el).removeClass('skeleton-text skeleton-effect-wave');

      var members = '<ul>';
      if(res.data && res.data.members && res.data.members.length > 0) {
        for(var i=0; i<res.data.members.length; i++) {
          members += '<li>'+res.data.members[i].Name+'</li>';
        }
      } else {
        members += '<li class="text-align-center" style="padding: 10px !important">KOSONG</li>';
      }
      members += '</ul>';
      $$('[data-label=list-member]', el).html(members);
      /* List Member */

      /* List Tower */
      $$('[data-label=list-tower]', el).empty();
      $$('[data-label=list-tower]', el).removeClass('skeleton-text skeleton-effect-wave');

      var items = '<ul>';
      if(res.data && res.data.items && res.data.items.length > 0) {
        for(var i=0; i<res.data.items.length; i++) {
          items += '<li data-uniq='+param.id+' data-statuscode='+res.data.items[i].StatusCode+' '+(res.data.items[i].StatusColor?'style="background-color:'+res.data.items[i].StatusColor+';"':'')+'>';
          items += '<a href="/tower-detail/'+res.data.items[i].Uniq+'/" class="item-link item-content" data-force="true" data-ignore-cache="true">';
          items += '<div class="item-inner">';
          items += '<div class="item-title-row">';
          items += '<div class="item-title" '+(res.data.items[i].StatusColor?'style="color:#fff !important;"':'')+'>'+res.data.items[i].WorkTitle+' - '+res.data.items[i].WorkSeq+'</div>';
          items += '</div>';
          //items += '<div class="item-subtitle">Kelas '+res.data.items[i].WorkType2+'</div>';
          items += '<div class="item-text" '+(res.data.items[i].StatusColor?'style="color:#fff !important;"':'')+'><strong>'+res.data.items[i].WorkType1+'</strong> Ext. +<strong>'+res.data.items[i].WorkExt+' / Kelas <strong>'+res.data.items[i].WorkType2+'</strong></strong></div>';
          items += '</div>';
          items += '</a>';
          items += '<div class="sortable-handler"></div>';
          items += '</li>';
        }
      } else {
        items += '<li class="text-align-center" style="padding: 10px !important">KOSONG</li>';
      }
      items += '</ul>';
      $$('[data-label=list-tower]', el).html(items);
      //app.sortable.enable($$('[data-label=list-tower]', el));
      /* List Tower */

    } else {
      var toast = app.toast.create({
        text: res.error,
        icon: '<i class="icon material-icons md-only">error</i>',
        position: 'center',
        closeTimeout: 2000
      });
      toast.open();
    }
  }, function(){
    var toast = app.toast.create({
      text: 'Terjadi kesalahan pada server. Silakan coba kembali.',
      icon: '<i class="icon material-icons md-only">error</i>',
      position: 'center',
      closeTimeout: 2000
    });
    toast.open();
  }, function(){});
});

$$(document).on('page:afterin', '.page[data-name="tower-form"]', function (e, page) {
  var el = page.el;
  var param = page.route.params;
  var auth = getLocalStorage('auth');

  $('[data-label=btn-submit]', el).unbind('click').click(function(){
    if(!app.input.validateInputs($('form[data-label=form]', el))) {
      app.dialog.alert('Harap isi form dengan benar.');
      return false;
    }

    var input = {
      WorkTitle: $('[name=WorkTitle]', el).val(),
      WorkCoord: $('[name=WorkCoord]', el).val(),
      WorkType1: $('[name=WorkType1]', el).val(),
      WorkType2: $('[name=WorkType2]', el).val(),
      WorkExt: $('[name=WorkExt]', el).val(),
      CreatedBy: auth.Email,
    }
    app.preloader.show();
    getData('tower-create/'+param.id, input, function(res) {
      res = JSON.parse(res);
      if(res.error == 0) {
        app.view.current.router.back();
      } else {
        app.dialog.alert(res.error);
        return false;
      }
    }, function(){

    }, function(){
      app.preloader.hide();
    });
  });
});

$$(document).on('page:afterin', '.page[data-name="tower-detail"]', function (e, page) {
  var el = page.el;
  var param = page.route.params;
  var auth = getLocalStorage('auth');

  $('[data-label=btn-progress1]', el).attr('href',app.view.current.router.generateUrl({name:'tower-progress',params:{id:param.id,tipe:'Pondasi'}}));
  $('[data-label=btn-progress2]', el).attr('href',app.view.current.router.generateUrl({name:'tower-progress',params:{id:param.id,tipe:'Erection'}}));

  $('a[data-label=btn-report]', el).unbind('click').click(function(){
    var content = '<div class="dialog-input-field input margin-top"><div class="item-input-wrap"><textarea rows="4"></textarea></div></div>';
    var dialogForm = app.dialog.create({
      closeByBackdropClick: true,
      destroyOnClose: true,
      title: 'Laporan Kendala',
      text: 'Uraikan Isu / Kendala:',
      content: content,
      buttons: [
        {text: 'BATAL', close: true, cssClass: 'color-gray'},
        {
          text: 'SUBMIT',
          close: false,
          onClick: function(dialog, e){
            var val_ = $$('textarea', dialog.el).val();
            getData('tower-report-add/'+param.id, {UserName: auth.Email, WorkRepMessage: val_}, function(res) {
              res = JSON.parse(res);
              if(res.error == 0) {

              } else {
                var toast = app.toast.create({
                  text: res.error,
                  icon: '<i class="icon material-icons md-only">error</i>',
                  position: 'center',
                  closeTimeout: 2000
                });
                toast.open();
              }
            }, function(){
              var toast = app.toast.create({
                text: 'Terjadi kesalahan pada server. Silakan coba kembali.',
                icon: '<i class="icon material-icons md-only">error</i>',
                position: 'center',
                closeTimeout: 2000
              });
              toast.open();
            }, function(){
              dialog.close();
              app.view.current.router.refreshPage();
            });
          }
        }
      ]
    });
    dialogForm.open();
  });

  $('a[data-link-form]', el).unbind('click').click(function(){
    if($(this).data('allow-authorized')!=1) {
      return false;
    } else if($(this).data('allow-change')!=1 && auth.ID_Role == ROLE_PENGAWAS) {
      return false;
    }

    var form = $(this).data('link-form');
    var val = $(this).data('link-value');
    var prop = $(this).data('link-prop');
    var prompt = $(this).data('link-prompt');
    var type = $(this).data('link-type');

    var content = '<div class="dialog-input-field input"><input type="text" class="dialog-input" value="'+val+'" /></div>';
    if(type) {
      if(type == 'select') {
        var opt = $(this).data('link-opt');
        var opts = '';
        if(opt) {
          opt = opt.split(",");
          for(var i=0; i<opt.length; i++) {
            var sel = (val&&val==opt[i]?'checked':'');
            //opts += '<option value="'+opt[i]+'" '+sel+'>'+opt[i]+'</option>';
            //opts += '<div class="col"><label class="radio"><input type="radio" name="radio" /><i class="icon-radio"></i>&nbsp;'+opt[i]+'</label></div>';
            opts += '<li>';
            opts += '<label class="item-radio item-radio-icon-start item-content">';
            opts += '<input type="radio" name="radio" value="'+opt[i]+'" '+sel+' />';
            opts += '<i class="icon icon-radio"></i><div class="item-inner"><div class="item-title">'+opt[i]+'</div></div>';
            opts += '</label>';
            opts += '</li>';
          }
        }
        content = '<div class="list"><ul>'+opts+'</ul></div>';
      } else if(type == 'date')  {
        content = '<div class="dialog-input-field input"><input type="date" class="dialog-input" value="'+val+'" /></div>';
      }
    }

    var dialogForm = app.dialog.create({
      closeByBackdropClick: true,
      destroyOnClose: true,
      title: form,
      text: (prompt?prompt:'Silakan isi kolom dibawah'),
      content: content,
      buttons: [
        {text: 'BATAL', close: true, cssClass: 'color-gray'},
        {
          text: 'SIMPAN',
          close: false,
          onClick: function(dialog, e){
            var val_ = $$('input', dialog.el).val();
            if(type=='select') {
              val_ = $$('input[type=radio]:checked', dialog.el).val();
            }
            getData('tower-change/'+param.id, {prop: prop, val: val_}, function(res) {
              res = JSON.parse(res);
              if(res.error == 0) {

              } else {
                var toast = app.toast.create({
                  text: res.error,
                  icon: '<i class="icon material-icons md-only">error</i>',
                  position: 'center',
                  closeTimeout: 2000
                });
                toast.open();
              }
            }, function(){
              var toast = app.toast.create({
                text: 'Terjadi kesalahan pada server. Silakan coba kembali.',
                icon: '<i class="icon material-icons md-only">error</i>',
                position: 'center',
                closeTimeout: 2000
              });
              toast.open();
            }, function(){
              dialog.close();
              app.view.current.router.refreshPage();
            });
          }
        }
      ]
    });
    dialogForm.open();
  });

  $('[data-label=btn-delete]', el).unbind('click').click(function(){
    app.dialog.confirm('Apakah anda yakin ingin menghapus tower ini?', 'Hapus', function(){
      app.preloader.show();
      getData('tower-delete/'+param.id, {}, function(res) {
        res = JSON.parse(res);
        if(res.error == 0) {
          app.view.current.router.back();
        } else {
          app.dialog.alert(res.error);
          return false;
        }
      }, function(){
        var toast = app.toast.create({
          text: 'Terjadi kesalahan pada server. Silakan coba kembali.',
          icon: '<i class="icon material-icons md-only">error</i>',
          position: 'center',
          closeTimeout: 2000
        });
        toast.open();
      }, function(){
        app.preloader.hide();
      });
    });
    return false;
  });

  $('a[data-link-form]', el).data('allow-authorized', 0);
  getData('tower-detail/'+param.id, {UserName:auth.Email}, function(res) {
    res = JSON.parse(res);
    if(res.error == 0) {
      if(res.isAuthorized) {
        $('a[data-link-form]', el).data('allow-authorized', 1);
      }
      if(res.isAuthorized && auth.ID_Role == ROLE_PENGGUNA) {
        $$('[data-label=btn-delete]').show();
      }

      if(res.data) {
        var startdate1='',startdate1_='';
        var startdate2='',startdate2_='';
        var startdate3='',startdate3_='';
        if(res.data.WorkStartDate1) {
          startdate1=moment(res.data.WorkStartDate1).format('DD/MM/YYYY');
          startdate1_=res.data.WorkStartDate1;
        } else {
          startdate1='<span style="font-style: italic;">belum diisi</span>';
        }
        if(res.data.WorkStartDate2) {
          startdate2=moment(res.data.WorkStartDate2).format('DD/MM/YYYY');
          startdate2_=res.data.WorkStartDate2;
        } else {
          startdate2='<span style="font-style: italic;">belum diisi</span>';
        }
        if(res.data.WorkStartDate3) {
          startdate3=moment(res.data.WorkStartDate3).format('DD/MM/YYYY');
          startdate3_=res.data.WorkStartDate3;
        } else {
          startdate3='<span style="font-style: italic;">belum diisi</span>';
        }

        $$('a[data-link-form', el).attr('data-link-form',res.data.WorkTitle);
        $$('[data-label=title]', el).removeClass('skeleton-text skeleton-effect-wave').html(res.data.WorkTitle).closest('a.item-content').attr('data-link-value',res.data.WorkTitle);
        $$('[data-label=work-type1]', el).removeClass('skeleton-text skeleton-effect-wave').html(res.data.WorkType1).closest('a.item-content').attr('data-link-value',res.data.WorkType1);
        $$('[data-label=work-type2]', el).removeClass('skeleton-text skeleton-effect-wave').html(res.data.WorkType2).closest('a.item-content').attr('data-link-value',res.data.WorkType2);
        $$('[data-label=work-ext]', el).removeClass('skeleton-text skeleton-effect-wave').html(res.data.WorkExt).closest('a.item-content').attr('data-link-value',res.data.WorkExt);
        $$('[data-label=work-statuslahan]', el).removeClass('skeleton-text skeleton-effect-wave').html(res.data.WorkStatus).closest('a.item-content').attr('data-link-value',res.data.WorkStatus);

        $$('[data-label=work-startdate1]', el).removeClass('skeleton-text skeleton-effect-wave').html(startdate1).closest('a.item-content').attr('data-link-value',startdate1_);
        $$('[data-label=work-startdate2]', el).removeClass('skeleton-text skeleton-effect-wave').html(startdate2).closest('a.item-content').attr('data-link-value',startdate2_);
        $$('[data-label=work-startdate3]', el).removeClass('skeleton-text skeleton-effect-wave').html(startdate3).closest('a.item-content').attr('data-link-value',startdate3_);
        $$('[data-label=work-is-adiktif]', el).removeClass('skeleton-text skeleton-effect-wave').html(res.data.WorkIsAdiktif&&res.data.WorkIsAdiktif=='1'?'Ya':'Tidak').closest('a.item-content').attr('data-link-value',res.data.WorkIsAdiktif&&res.data.WorkIsAdiktif=='1'?'Ya':'Tidak');

        //$$('[data-label=work-progress1]', el).attr('data-value',parseFloat(res.data.Progress1)).attr('data-value-text',);
        //$$('[data-label=work-progress2]', el).attr('data-value',parseFloat(res.data.Progress2)).attr('data-value-text',numeral(parseFloat(res.data.Progress2)).format('0,00')+'%');
        var gauge1 = app.gauge.get('[data-label=work-progress1]');
        var gauge1_ = app.gauge.get('[data-label=work-progressdev1]');
        var gauge2 = app.gauge.get('[data-label=work-progress2]');
        var gauge2_ = app.gauge.get('[data-label=work-progressdev2]');
        var gauge3 = app.gauge.get('[data-label=work-progress3]');
        var colordev1 = '#8e8e93';
        var colordev2 = '#8e8e93';

        if(res.data.ProgressStd1 && res.data.ProgressStd1 >= 100) colordev1='#000000';
        else if(res.data.ProgressDev1 && res.data.ProgressDev1 > 50) colordev1='#ff3b30';
        else if(res.data.ProgressDev1 && res.data.ProgressDev1 >= 25) colordev1='#ffcc00';
        else if(res.data.ProgressDev1 && (res.data.ProgressDev1 > 0 && res.data.ProgressDev1 < 25)) colordev1='#4cd964';

        if(res.data.ProgressStd2 && res.data.ProgressStd2 >= 100) colordev2='#000000';
        else if(res.data.ProgressDev2 && res.data.ProgressDev2 > 50) colordev2='#ff3b30';
        else if(res.data.ProgressDev2 && res.data.ProgressDev2 >= 25) colordev2='#ffcc00';
        else if(res.data.ProgressDev2 && (res.data.ProgressDev2 > 0 && res.data.ProgressDev2 < 25)) colordev2='#4cd964';

        gauge1.update({
          value: parseFloat(res.data.Progress1)/100,
          valueText: numeral(parseFloat(res.data.Progress1)).format('0.00')+'%'
        });
        gauge2.update({
          value: parseFloat(res.data.Progress2)/100,
          valueText: numeral(parseFloat(res.data.Progress2)).format('0.00')+'%'
        });

        gauge1_.update({
          value: parseFloat(res.data.ProgressDev1)/100,
          valueText: numeral(parseFloat(res.data.ProgressDev1)).format('0.00')+'%',
          valueTextColor: colordev1,
          borderColor: colordev1
        });
        gauge2_.update({
          value: parseFloat(res.data.ProgressDev2)/100,
          valueText: numeral(parseFloat(res.data.ProgressDev2)).format('0.00')+'%',
          valueTextColor: colordev2,
          borderColor: colordev2
        });

        if(res.data.DurBeton) {
          gauge3.update({
            value: parseFloat(res.data.DurBeton)/28,
            valueText: numeral(parseFloat(res.data.DurBeton)).format('0.00'),
          });
        }
      }

      if(res.reports && res.reports.length > 0) {
        var rep = '';
        rep += '<ul>';
        for(var i=0; i<res.reports.length; i++) {
          rep += '<li>';
          rep += '<div class="item-content">';
          rep += '<div class="item-inner item-cell">';
          rep += '<div class="item-title-row">';
          rep += '<div class="item-title"><strong>'+res.reports[i].Name+'</strong></div>';
          rep += '<div class="item-after">'+moment(res.reports[i].CreatedOn).format('DD/MM/YYYY')+'</div>';
          rep += '</div>';
          //rep += '<div class="item-subtitle">___</div>';
          //rep += '<div class="item-text" style="overflow: visible !important; text-overflow: unset !important">'+res.reports[i].WorkRepMessage+'</div>';
          rep += '<div class="item-row"><div class="item-cell">'+res.reports[i].WorkRepMessage+'</div></div>';
          rep += '</div>';
          rep += '</div>';
          rep += '</li>';
        }
        rep += '</ul>';
        $$('[data-label=list-report]', el).html(rep);
      } else {
        $$('[data-label=list-report]', el).html('<ul><li><div class="item-content"><div class="item-inner item-cell"><div class="item-row"><div class="item-cell" style="text-align: center"><small class="text-color-gray">KOSONG</small></div></div></div></div></li></ul>');
      }
    } else {
      var toast = app.toast.create({
        text: res.error,
        icon: '<i class="icon material-icons md-only">error</i>',
        position: 'center',
        closeTimeout: 2000
      });
      toast.open();
    }
  }, function(){
    var toast = app.toast.create({
      text: 'Terjadi kesalahan pada server. Silakan coba kembali.',
      icon: '<i class="icon material-icons md-only">error</i>',
      position: 'center',
      closeTimeout: 2000
    });
    toast.open();
  }, function(){});
});

$$(document).on('page:afterin', '.page[data-name="tower-progress"]', function (e, page) {
  var el = page.el;
  var param = page.route.params;
  var auth = getLocalStorage('auth');

  $$('[data-label=title]', el).html('Progress '+param.tipe);

  getData('tower-progress/'+param.id+'/'+param.tipe, {UserName: auth.Email}, function(res) {
    res = JSON.parse(res);
    if(res.error == 0) {
      $$('[data-label=work-title]', el).removeClass('skeleton-text skeleton-effect-wave').html(res.data.WorkTitle);
      $$('[data-label=work-type1]', el).removeClass('skeleton-text skeleton-effect-wave').html(res.data.WorkType1);
      $$('[data-label=work-type2]', el).removeClass('skeleton-text skeleton-effect-wave').html(res.data.WorkType2);
      $$('[data-label=work-ext]', el).removeClass('skeleton-text skeleton-effect-wave').html(res.data.WorkExt);

      var progress = '';
      if(res.keys && res.items) {
        for(var i=0; i<res.keys.length; i++) {
          progress += '<div class="block-title">'+res.keys[i].WorkDetType+'</div>';

          var items = res.items.filter(x => x.WorkDetType == res.keys[i].WorkDetType);
          if(items) {
            progress += '<div class="list">';
            progress += '<ul>';
            for(var n=0; n<items.length; n++) {
              var checked_ = items[n].WorkDetIsComplete==1?'checked="checked"':'';
              var uploaded_ = items[n].WorkDetImages?'checked="checked"':'';
              progress += '<li>';
              /*progress += '<label class="item-checkbox item-content">';
              progress += '<input type="checkbox" value="1" data-label="input-work-checklist" '+checked_+' />';
              progress += '<i class="icon icon-checkbox"></i>';
              progress += '<div class="item-inner"><div class="item-title">'+items[n].WorkDetName+'</div></div>';
              progress += '</label>';*/
              progress += '<div class="item-content">';
              progress += '<div class="item-inner">'
              progress += '<div class="item-title" style="white-space: normal !important">'+items[n].WorkDetName+'</div>';
              progress += '<div class="item-after">';
              progress += '<input type="file" accept="image/*" name="file" data-uniq="'+items[n].Uniq+'" data-label="input-file-progress" style="display: none"  />';
              progress += '<button class="col button button-small button-round '+(items[n].WorkDetIsComplete==1?'':'color-gray')+'" data-label="btn-work-checklist" data-uniq="'+items[n].Uniq+'" data-tipe="'+items[n].WorkDetType+'" data-name="'+items[n].WorkDetName+'"><i class="icon material-icons md-only">check_circle_outline</i></button>';
              progress += '<button class="col button button-small button-round '+(items[n].WorkDetImages?'':'color-gray')+'"  data-label="btn-work-image" data-url="'+items[n].WorkDetImages+'" data-uniq="'+items[n].Uniq+'"><i class="icon material-icons md-only">image</i></button>';
              progress += '<button class="col button button-small button-round"  data-label="btn-work-upload" data-uniq="'+items[n].Uniq+'"><i class="icon material-icons md-only">add_a_photo</i></button>';
              progress += '</div>';
              progress += '</div>';
              progress += '</div>';
              progress += '</li>';
            }
            progress += '</ul>';
            progress += '</div>';
          }
        }
      }
      $$('[data-label=progress-item]', el).html(progress);
      $('input[type=file][data-label=input-file-progress]', el).change(function(){
        var uniq_ = $(this).data('uniq');
        var formData = new FormData();
        formData.append("file", this.files[0]);

        app.preloader.show();
        app.request.post(URL_API+'tower-progress-upload/'+uniq_, formData).then(function (res) {
          app.preloader.hide();
          var res = JSON.parse(res.data);
          console.log(res);
          if(res.error == 0) {
            var toast = app.toast.create({
              text: 'Gambar berhasil dikirim.',
              icon: '<i class="icon material-icons md-only">check_circle</i>',
              position: 'center',
              closeTimeout: 2000
            });
            toast.open();
            app.view.current.router.refreshPage();
          } else {
            app.dialog.alert(res.error);
          }
        }).catch(function (err) {
          console.log(err);
          app.preloader.hide();
          app.dialog.alert(err.message);
        });
        //$(this).closest('form').submit();
      });
      $('button[data-label=btn-work-checklist]', el).unbind('click').click(function(){
        if(!res.isAuthorized) {
          return false;
        }

        var uniq_ = $(this).data('uniq');
        var tipe_ = $(this).data('tipe');
        var name_ = $(this).data('name');
        if(uniq_) {
          app.dialog.confirm('Ubah status pekerjaan <b>'+tipe_+'</b> - <b>'+name_+'</b> ?', 'Progress '+param.tipe, function(){
            app.preloader.show();
            /* Change Status */
            getData('tower-progress-changestatus/'+uniq_, {}, function(res) {
              res = JSON.parse(res);
              if(res.error == 0) {
                //app.dialog.alert(res.success);
                if(res.data && res.data.WorkID && res.data.isPondasiComplete) {
                  var opts = '';
                  opts += '<li>';
                  opts += '<label class="item-radio item-radio-icon-start item-content">';
                  opts += '<input type="radio" name="radio" value="1" checked />';
                  opts += '<i class="icon icon-radio"></i><div class="item-inner"><div class="item-title">Ya</div></div>';
                  opts += '</label>';
                  opts += '</li>';
                  opts += '<li>';
                  opts += '<label class="item-radio item-radio-icon-start item-content">';
                  opts += '<input type="radio" name="radio" value="0" checked />';
                  opts += '<i class="icon icon-radio"></i><div class="item-inner"><div class="item-title">Tidak</div></div>';
                  opts += '</label>';
                  opts += '</li>';

                  var content = '<div class="dialog-input-field input margin-top"><div class="item-input-wrap"><div class="list"><ul>'+opts+'</ul></div></div></div>';
                  var dialogForm = app.dialog.create({
                    closeByBackdropClick: true,
                    destroyOnClose: true,
                    title: 'Pilih Campuran Beton',
                    text: 'Apakah pondasi dilakukan pencampuran dengan zat adiktif?',
                    content: content,
                    buttons: [
                      {
                        text: 'SUBMIT',
                        close: false,
                        onClick: function(dialog, e){
                          var val_ = $$('input[type=radio]:checked', dialog.el).val();
                          getData('tower-update-umurbeton/'+res.data.WorkID, {WorkIsAdiktif:val_}, function(res) {
                            res = JSON.parse(res);
                            if(res.error == 0) {

                            } else {
                              var toast = app.toast.create({
                                text: res.error,
                                icon: '<i class="icon material-icons md-only">error</i>',
                                position: 'center',
                                closeTimeout: 2000
                              });
                              toast.open();
                            }
                          }, function(){
                            var toast = app.toast.create({
                              text: 'Terjadi kesalahan pada server. Silakan coba kembali.',
                              icon: '<i class="icon material-icons md-only">error</i>',
                              position: 'center',
                              closeTimeout: 2000
                            });
                            toast.open();
                          }, function(){
                            dialog.close();
                            app.view.current.router.refreshPage();
                          });
                        }
                      }
                    ]
                  });
                  dialogForm.open();
                }
              } else {
                app.dialog.alert(res.error);
              }
            }, function(){
              app.dialog.alert('Terjadi kesalahan pada server. Silakan coba kembali.');
            }, function(){
              app.preloader.hide();
              app.view.current.router.refreshPage();
            });
            /* Change Status */
          });
        }
      });

      $('button[data-label=btn-work-upload]', el).unbind('click').click(function(){
        if(!res.isAuthorized) {
          return false;
        }

        var uniq_ = $(this).data('uniq');
        $('input[type=file][data-uniq='+uniq_+']').click();
        /*navigator.camera.getPicture(function(fileURI){
          var ftoptions = new FileUploadOptions();
          ftoptions.fileKey = "file";
          ftoptions.fileName = fileURI.substr(fileURI.lastIndexOf('/') + 1);
          ftoptions.mimeType = "image/jpeg";

          var ft = new FileTransfer();
          app.preloader.show();
          ft.upload(fileURI, URL_API+'tower-progress-upload/'+uniq_, function(r){
            console.log(r);
            app.preloader.hide();
            navigator.camera.cleanup();

            var res = JSON.parse(r.response);
            if(res.error == 0) {
              var toast = app.toast.create({
                text: 'Gambar berhasil dikirim.',
                icon: '<i class="icon material-icons md-only">check_circle</i>',
                position: 'center',
                closeTimeout: 2000
              });
              toast.open();
              app.view.current.router.refreshPage();
            } else {
              app.dialog.alert(res.error);
            }
          }, function(error){
            navigator.camera.cleanup();
            console.log(error);
            app.preloader.hide();
            app.dialog.alert('Gagal mengirim gambar.');
          }, ftoptions, true);
        }, function(message){
          app.dialog.alert(message);
        }, {
            quality: 100,
            destinationType: Camera.DestinationType.FILE_URI,
            sourceType: Camera.PictureSourceType.PHOTOLIBRARY,
            encodingType: Camera.EncodingType.JPEG
        });*/
      });

      $('button[data-label=btn-work-image]', el).unbind('click').click(function(){
        var uniq_ = $(this).data('uniq');
        var url_ = $(this).data('url').split(",");
        if(url_) {
          var photoBrowser = app.photoBrowser.create({
            photos: url_,
            popupCloseLinkText: 'Tutup',
            navbarOfText:'dari',
            on : {
              opened: function(){
                $$('.navbar-photo-browser>.navbar-inner', photoBrowser.el).prepend('<a class="link" data-label="btn-delete-photo" data-index="0"><i class="icon material-icons md-only">delete</i></a>');
                $$('.navbar-photo-browser>.navbar-inner>.title', photoBrowser.el).css('left', '0');

                $$('[data-label=btn-delete-photo]', photoBrowser.el).click(function(){
                  var index = $$(this).data('index');
                  if(index) {
                    app.dialog.confirm('Apakah anda yakin ingin menghapus foto ini?', 'Hapus Foto', function(){
                      getData('tower-progress-imgdel/'+uniq_, {idx: index}, function(res) {
                        res = JSON.parse(res);
                        if(res.error == 0) {

                        } else {
                          var toast = app.toast.create({
                            text: res.error,
                            icon: '<i class="icon material-icons md-only">error</i>',
                            position: 'center',
                            closeTimeout: 2000
                          });
                          toast.open();
                        }
                      }, function(){
                        var toast = app.toast.create({
                          text: 'Terjadi kesalahan pada server. Silakan coba kembali.',
                          icon: '<i class="icon material-icons md-only">error</i>',
                          position: 'center',
                          closeTimeout: 2000
                        });
                        toast.open();
                      }, function(){
                        photoBrowser.close();
                        app.view.current.router.refreshPage();
                      });
                    }, function(){

                    });
                  }
                  return false;
                });
              },
              slideChange: function(e) {
                $$('[data-label=btn-delete-photo]', photoBrowser.el).attr('data-index', e.activeIndex);
                $$('.navbar-photo-browser>.navbar-inner>.title', photoBrowser.el).css('left', '0');
              }
            }
          });
          photoBrowser.open();
        }
      })
    } else {
      var toast = app.toast.create({
        text: res.error,
        icon: '<i class="icon material-icons md-only">error</i>',
        position: 'center',
        closeTimeout: 2000
      });
      toast.open();
    }
  }, function(){
    var toast = app.toast.create({
      text: 'Terjadi kesalahan pada server. Silakan coba kembali.',
      icon: '<i class="icon material-icons md-only">error</i>',
      position: 'center',
      closeTimeout: 2000
    });
    toast.open();
  }, function(){});
});

$$(document).on('page:afterin', '.page[data-name="notification"]', function (e, page) {
  var el = page.el;
  var param = page.route.params;
  var auth = getLocalStorage('auth');
  var notif = getLocalStorage('notification');
  if(notif && notif.length>0) {
    var notifs = '';
    notifs += '<ul>';
    for(var i=0; i<notif.length; i++) {
      var icon_ = 'img/icon-notification.png';
      if(notif[i].MessageType == 'Peringatan Berbahaya') {
        icon_ = 'img/icon-danger.png';
      }

      notifs += '<li>';
      notifs += '<a href="/tower-detail/'+notif[i].WorkID+'/" class="item-link item-content" data-force="true" data-ignore-cache="true">';
      notifs += '<div class="item-media">';
      notifs += '<img src="'+icon_+'" width="45" />';
      notifs += '</div>';
      notifs += '<div class="item-inner">';
      notifs += '<div class="item-title-row">';
      notifs += '<div class="item-title">'+notif[i].MessageType+'</div>';
      notifs += '</div>';
      notifs += '<div class="item-subtitle">'+notif[i].MessageSeverity+'</div>';
      notifs += '<div class="item-text"><strong>'+notif[i].PrName+'</strong> : '+notif[i].WorkTitle+'</div>';
      notifs += '</div>';
      notifs += '</a>';
      notifs += '</li>';
    }
    notifs += '</ul>';
    $$('[data-label=list-notification]', el).removeClass('skeleton-text skeleton-effect-wave').html(notifs).show();
  } else {
    $$('[data-label=list-notification]', el).removeClass('skeleton-text skeleton-effect-wave').empty().hide();
  }
});
