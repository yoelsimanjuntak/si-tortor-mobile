routes = [
  /*{
    path: '/tabs/',
    tabs: [
      {
        path: '/',
        id: 'view-home'
      },
      {
        path: '/catalog/',
        id: 'view-catalog'
      },
      {
        path: '/order/',
        id: 'view-order'
      },
    ],
  },*/
  {
    name:'home',
    path: '/',
    url: './index.html'
  },
  {
    name:'login',
    path: '/login/',
    url: './pages/login.html'
  },
  {
    name:'project',
    path: '/project/:id/',
    url: './pages/project.html'
  },
  {
    name:'project-form',
    path: '/project-form/',
    url: './pages/project-form.html'
  },
  {
    name:'project-detail',
    path: '/project-detail/:id/',
    url: './pages/project-detail.html'
  },
  {
    name:'tower-form',
    path: '/tower-form/:id/',
    url: './pages/tower-form.html'
  },
  {
    name:'tower-detail',
    path: '/tower-detail/:id/',
    url: './pages/tower-detail.html'
  },
  {
    name:'tower-progress',
    path: '/tower-progress/:id/:tipe/',
    url: './pages/tower-progress.html'
  },
  {
    name:'notification',
    path: '/notification/',
    url: './pages/notification.html'
  },
  // Default route (404 page). MUST BE THE LAST
  /*{
      path: '(.*)',
      url: './pages/404.html'
  },*/
];
